try:
    import tkinter as tk

except ImportError:
    import Tkinter as tk

import requests
import json
from datetime import date, timedelta


class BTCPrice:
    def __init__(self, parent):
        # --- WIDGETS 
        self.btc_eur = tk.Label(parent, text="BTC / EUR", font="Arial 20", width=0)
        self.btc_usd = tk.Label(parent, text="BTC / USD", font="Arial 20", width=0)

        self.label_1 = tk.Label(parent, text="0", font="Arial 30", width=0, fg="#26d30c")
        self.label_2 = tk.Label(parent, text="0", font="Arial 30", width=0, fg="#26d30c")

        self.eur_diff = tk.Label(parent, text="", font='Arial 20', width= 0)
        self.usd_diff = tk.Label(parent, text="", font='Arial 20', width= 0)
        self.label_diff = tk.Label(parent, text="", font="Arial 5", width=0)
        self.value_diff = tk.Label(parent, text="0", font="Arial 30", width=0)
        self.message = tk.Label(parent, text="Prices are updated every minute", font="Arial 10", width=0)

        # --- POSITIONS
        self.btc_eur.place(x=190, y=0) # BTC/ EUR
        self.label_1.place(x=100, y=40)    # PRECIO EN EUR
        self.eur_diff.place(x= 300, y=45) # DIFRENCIA EN EUR
        self.btc_usd.place(x=190, y=100) # BTC / USD
        self.label_2.place(x=100, y=150) # PRECIO EN USD
        self.usd_diff.place(x= 300, y=155) # DIFERENCIA EN USD
        self.value_diff.place(x=175, y = 200)
        self.message.place(x=20, y = 240)

        self.label_1.after(1000, self.getPriceEur)        
        self.label_2.after(1000, self.getPriceUsd)
        self.value_diff.after(1000, self.getDiff)
        self.eur_diff.after(1000, self.getEurDiff)
        self.usd_diff.after(1000, self.getUsdDiff)

        # --- VARIABLES
        self.previousPriceEur = 0.0
        self.previousPriceUsd = 0.0
        self.yesterday = (date.today() - timedelta(1)).strftime('%Y-%m-%d')
        self.yesterdays_price_eur = self.getYesterdaysPrice('EUR')
        self.yesterdays_price_usd = self.getYesterdaysPrice('USD')


    # --- METHODS
    def getYesterdaysPrice(self, currency):
        '''gets yesterdays bitcoin value'''
        if currency == 'USD':
            url = 'https://api.coindesk.com/v1/bpi/historical/close.json?for=yesterday'
        elif currency == 'EUR':
            url = 'https://api.coindesk.com/v1/bpi/historical/close.json?currency=EUR&for=yesterday'

        r = requests.get(url)
        data = r.text
        parsed = json.loads(data)
        return float(parsed['bpi'][self.yesterday])


    def getPriceEur(self):
        '''gets the last bitcoin value in eur'''

        url = 'https://api.coindesk.com/v1/bpi/currentprice.json'
        r = requests.get(url)
        data = r.text
        parsed = json.loads(data)
        if self.previousPriceEur < float(parsed['bpi']['EUR']['rate_float']):
            new_fg = '#26d30c'
        elif self.previousPriceEur > float(parsed['bpi']['EUR']['rate_float']):
            new_fg = '#ff150c'
        else:
            new_fg = '#000000'

        self.previousPriceEur = float(parsed['bpi']['EUR']['rate_float'])  
        current_value_eur = round(parsed['bpi']['EUR']['rate_float'], 4)
        self.label_1.configure(text="%s" % current_value_eur, fg=new_fg)
        self.label_1.after(1000, self.getPriceEur)

    
    def getEurDiff(self):
        diff = round(self.previousPriceEur - self.yesterdays_price_eur, 4)
        if diff > 0:
            new_fg = '#26d30c'
        elif diff < 0:
            new_fg = '#ff150c'
        else:
            new_fg = '#000000'

        self.eur_diff.configure(text="%s" % str(diff), fg=new_fg)
        self.eur_diff.after(1000, self.getEurDiff)

    def getUsdDiff(self):
        diff = round(self.previousPriceUsd - self.yesterdays_price_usd, 4)
        if diff > 0:
            new_fg = '#26d30c'
        elif diff < 0:
            new_fg = '#ff150c'
        else:
            new_fg = '#000000'

        self.usd_diff.configure(text="%s" % str(diff), fg=new_fg)
        self.usd_diff.after(1000, self.getUsdDiff)


    def getPriceUsd(self):
        '''gets the last bitcoin value in usd'''       
        url = 'https://api.coindesk.com/v1/bpi/currentprice.json'
        r = requests.get(url)
        data = r.text
        parsed = json.loads(data)
        if self.previousPriceUsd < float(parsed['bpi']['USD']['rate_float']):
            new_fg = '#26d30c'
        elif self.previousPriceUsd > float(parsed['bpi']['USD']['rate_float']):
            new_fg = '#ff150c'
        else:
            new_fg = '#000000'

        self.previousPriceUsd = float(parsed['bpi']['USD']['rate_float'])
        current_value_usd = round(parsed['bpi']['USD']['rate_float'], 4)
        self.label_2.configure(text="%s" % current_value_usd, fg=new_fg)
        self.label_2.after(1000, self.getPriceUsd)

    def getDiff(self):
        ''' calculates the percentage difference between yesyerday's and the current price '''
        diff = round(((float(self.previousPriceUsd) * 100 / float(self.yesterdays_price_usd)) - 100), 4)

        if diff > 0:
            new_fg = '#26d30c'
        elif diff < 0:
            new_fg = '#ff150c'
        else:
            new_fg = '#000000'
        
        self.value_diff.configure(text="%s" % (str(diff) + '%'), fg=new_fg)
        self.value_diff.after(1000, self.getDiff)

   

if __name__ == "__main__":
    root = tk.Tk()
    root.title('BTC Price')
    root.tk.call('wm', 'iconphoto', root._w, tk.PhotoImage(file='RaspBitcoin.png'))
    root.geometry("480x320")
    app = BTCPrice(root)
    root.mainloop()
