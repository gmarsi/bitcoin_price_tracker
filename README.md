### ENGLISH

Bitcoin pricke tracker is a very simple desktop application than shows the current price of Bitcoin in euro and united states dollar currencies. 
It's designed for Raspberry although you can use it on Windows, Linux or even MacOS

To make it works open the terminal and type 'python init.py' in the working directory

You may need to install the 'requests' libray. Just type: pip install requests
More info about how to install this dependence: [here](https://stackoverflow.com/questions/30362600/how-to-install-requests-module-in-python-3-4-instead-of-2-7)

The size of the application window fits perfectly in 320x480 screens, although you can change it´s size

Enjoy

----------------------------------------------------------

### SPANISH

Bitcoin price tracker ees una aplicación muy sencilla que muestra el precio actual del Bitcoin en euros y en dolares estadounidenses. Está pensado para usarse
en una reaspberry, aunque se puede user en Wondows, Linux e incluso MacOS

Para ejecutarla, abre una terminal en el directorio dónde se encuentra el archivo "init.py", y escribe: python init.py

Probablemente, necesitarás instalar la librería "requests". Para ello escribe en la terminal: pip install requests
Más información sobre cómo instalar esta dependencia: [aquí](https://stackoverflow.com/questions/30362600/how-to-install-requests-module-in-python-3-4-instead-of-2-7)

El tamaño de la ventana de la aplicación encaja perfectamente en pantallas de 320 x 480, pero puedes modificarlo a tu gusto

Disfruta!
